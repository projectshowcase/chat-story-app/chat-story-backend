const assert = require('assert');
const app = require('../../../src/app');

describe('\'health/ready\' service', () => {
  it('registered the service', () => {
    const service = app.service('health/ready');

    assert.ok(service, 'Registered the service');
  });
});
