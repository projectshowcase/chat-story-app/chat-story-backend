const assert = require('assert');
const app = require('../../../src/app');

describe('\'health/live\' service', () => {
  it('registered the service', () => {
    const service = app.service('health/live');

    assert.ok(service, 'Registered the service');
  });
});
