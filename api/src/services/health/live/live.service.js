// Initializes the `health/live` service on path `/health/live`
const createService = require('./live.class.js');
const hooks = require('./live.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');
  const env = app.get('env');

  const options = {
    paginate,
    env
  };

  // Initialize our service with any options it requires
  app.use('/health/live', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('health/live');

  service.hooks(hooks);
};
