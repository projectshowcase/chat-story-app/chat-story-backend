// Initializes the `health/ready` service on path `/health/ready`
const createService = require('./ready.class.js');
const hooks = require('./ready.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');
  const env = app.get('env');

  const options = {
    paginate,
    env
  };

  // Initialize our service with any options it requires
  app.use('/health/ready', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('health/ready');

  service.hooks(hooks);
};
