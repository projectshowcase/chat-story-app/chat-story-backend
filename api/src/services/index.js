const healthLive = require('./health/live/live.service.js');
const healthReady = require('./health/ready/ready.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(healthLive);
  app.configure(healthReady);
};
